(defun alfabeta (stanje dubina alfa beta sledeci naPotezu)
    (cond ((zerop dubina) (evaluacija stanje naPotezu))                     
          ((eq sledeci 'max)
                (block maxBlock                                            
                    (let* ((vrednost alfa) 
                          (validniPotezi (pronadjiValidnePotezeZaIgraca matrica naPotezu '1))
                          (svaStanja (napraviListuMogucihStanja matrica validniPotezi)))      
                        (loop for stanje in svaStanja do   
                            (let* ((vPrim (alfabeta stanje (- dubina 1) vrednost beta (promeniSledeceg sledeci) (promeniIgracaNaPotezu naPotezu))))   
                                (if (> vPrim vrednost) (setf vrednost vPrim) )         
                                (if (> vrednost beta) (return-from maxBlock beta))
                            )
                        )
                        (return-from maxBlock vrednost)                           
                    )))
          ((eq sledeci 'min)
                (block minBlock                                            
                    (let* ((vrednost beta) 
                          (validniPotezi (pronadjiValidnePotezeZaIgraca matrica naPotezu '1))
                          (svaStanja (napraviListuMogucihStanja matrica validniPotezi)))                                   
                        (loop for stanje in svaStanja do  
                            (let* ((vPrim (alfabeta stanje (- dubina 1) alfa vrednost (promeniSledeceg sledeci) (promeniIgracaNaPotezu naPotezu))))   
                                (if (< vPrim vrednost) (setf vrednost vPrim))           
                                (if (< vrednost alfa) (return-from minBlock alfa)) 
                            )
                        )
                        (return-from minBlock vrednost)                           
                    )))
))

(defun promeniSledeceg (sledeci) (
    if (eq sledeci 'max) 'min 'max
))

(defun promeniIgracaNaPotezu (naPotezu) 
    (if (eq naPotezu 'X) 'O 'X)
)

(defun indeksNajveceVrednosti (listaVrednosti brElementa indeksNajveceg granicnaVrednost)
    (cond 
        ((null listaVrednosti) indeksNajveceg)
        ((> (car listaVrednosti) granicnaVrednost) (indeksNajveceVrednosti (cdr listaVrednosti) (1+ brElementa) brElementa (car listaVrednosti)))
        (t (indeksNajveceVrednosti (cdr listaVrednosti) (1+ brElementa) indeksNajveceg granicnaVrednost))
    )
)

(defun vratiStanje (matrica dubina naPotezu)
    (let* (
        (validniPotezi (pronadjiValidnePotezeZaIgraca matrica naPotezu '1))
        (listaStanja (napraviListuMogucihStanja matrica validniPotezi))
        (vrednosti 
            (loop for stanje in listaStanja collect
                (alfabeta stanje (1- dubina) '-1000 '1000 'min (promeniIgracaNaPotezu naPotezu))
            )))
        (nth (indeksNajveceVrednosti vrednosti '0 '0 '-1000) listaStanja)
    )
)
